Name:           pcaudiolib
Version:        1.1
Release:        3
Summary:        Portable C Audio Library
License:        GPLv3+
URL:            https://github.com/rhdunn/pcaudiolib
Source0:        %{url}/archive/%{version}.tar.gz

BuildRequires:  gcc make autoconf automake libtool pkgconfig alsa-lib-devel pulseaudio-libs-devel

%description
The Pcaudiolib package provides a C API to different audio devices.

%package devel
Summary: Libraries and header files for using pcaudiolib
Requires: %{name} = %{version}-%{release}

%description devel
Libraries and header files for using pcaudiolib.

%prep
%autosetup
find . -type d -name TPCircularBuffer | xargs rm -rf

%build
./autogen.sh
%configure --without-coreaudio
%make_build

%install
%make_install
%delete_la_and_a

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%license COPYING
%doc README.md AUTHORS CHANGELOG.md
%{_libdir}/libpcaudio.so.*

%files devel
%{_libdir}/libpcaudio.so
%dir %{_includedir}/pcaudiolib
%{_includedir}/pcaudiolib/audio.h

%changelog
* Thu Dec 12 2019 lijin Yang <yanglijin@huawei.com> - 1.1-3
- init package

